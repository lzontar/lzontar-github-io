import { LzontarGithubIoPage } from './app.po';

describe('lzontar.github.io App', function() {
  let page: LzontarGithubIoPage;

  beforeEach(() => {
    page = new LzontarGithubIoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
